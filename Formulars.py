import math
import numpy as np

def Linear(beta_1, X):
    y = 1 - (beta_1 * X)
    return y

def Linear(beta_0, beta_1, X):
    y = beta_0 - (beta_1 * X)
    return y

def Square_root(beta_1, X):
    y = 1 - (beta_1*math.sqrt(X))
    return y

def Power_law(beta_1, beta_2, X):
    y = 1 - (beta_1 * math.pow(X,beta_2))
    return y

def Power_law(beta_0, beta_1, beta_2, X):
    y = beta_0 - (beta_1 * math.pow(X,beta_2))
    return y

def Stretched_exponential(beta_0, beta_1, beta_2, X):
    y = beta_0 * math.exp(beta_1 * (math.pow(X, beta_2)))
    return y

def Stretched_exponential_cuervo(beta_0, beta_1, beta_2, X):
    y = beta_0 * math.exp(math.pow((X/beta_1), beta_2))
    return y

def Logarithm(beta_1, X):
    y = 1 - beta_1 * math.log10(X)
    return y

def Sigmoidal( beta_1, beta_2, beta_3, X):
    y = 1 - 2 * beta_1 * ((1/2) - 1/(1+math.pow(math.exp(beta_2*X), beta_3)))
    return y

def Polynomial(beta_1, beta_2, X_1, X_2):
    minuend_1 = 0
    minuend_2 = 0
    for i in range (0, 3):
        minuend_1 += beta_1[i] * math.pow(X_1, i)

        minuend_2 += beta_2[i] * math.pow(X_2, i)
   
    y = 1 - minuend_1 - minuend_2
    return y

